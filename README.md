# Welcome to George FE Test task

This small web app is a task for a frontend developer interview at Erste. It is a searchable list of currency exchange rates fetched from an API.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Deployment

The app is deployed via Netlify and you can check it out [here](https://bright-druid-df5fcb.netlify.app/).
