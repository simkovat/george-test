import { Header } from './components/Header/Header';
import css from './App.module.css';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ExchangeRates } from './pages/ExchangeRates/ExchangeRates';
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className={css.wrapper}>
        <Header />
        <ExchangeRates />
      </div>
    </QueryClientProvider>
  );
}

export default App;
