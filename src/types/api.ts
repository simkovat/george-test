export interface RatesData {
  baseCurrency: string;
  comparisonDate: string;
  fx: RatesItem[];
  institute: number;
  lastUpdated: string;
}

export interface RatesItem {
  banknoteRate?: Rate;
  currency: string;
  exchangeRate?: Rate;
  flags?: Flag[];
  nameI18N?: string;
  precision: number;
  denominations?: number[];
}

interface Rate {
  buy: number;
  indicator: number;
  lastModified: string;
  middle: number;
  sell?: number;
}

export enum Flag {
  Provided = 'provided',
  TradingProhibited = 'tradingProhibited'
}
