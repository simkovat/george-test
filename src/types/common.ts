import { RatesItem } from './api';

export interface RatesItemEnhanced extends RatesItem {
  countryIso: string;
  countryName: string;
}
