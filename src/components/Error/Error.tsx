import { FC } from 'react';
import css from './Error.module.css';

export const Error: FC<{ text: string }> = ({ text }) => {
  return (
    <div className={css.container}>
      <div className={css.error}>{text}</div>
    </div>
  );
};
