import { ChangeEvent, FC } from 'react';
import { useTranslation } from 'react-i18next';
import css from './Search.module.css';

interface Props {
  value: string;
  setValue: (value: string) => void;
}

export const Search: FC<Props> = ({ value, setValue }) => {
  const { t } = useTranslation();
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  return (
    <div className={css.wrapper}>
      <label htmlFor="search">{t('search')}: </label>
      <input value={value} onChange={handleChange} id="search" data-testid="search-input" />
    </div>
  );
};
