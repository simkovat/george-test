import { FC } from 'react';
import css from './CurrencyItem.module.css';
import { RatesItemEnhanced } from '../../types/common';

interface Props {
  data: RatesItemEnhanced;
  baseCurrency: string;
}

export const CurrencyItem: FC<Props> = ({ data, baseCurrency }) => {
  return (
    <div className={css.wrapper}>
      <div className={css.countryInfoWrapper}>
        <img
          className={css.flag}
          src={`../../assets/flags/${data.countryIso}.png`}
          alt={data.currency}
        />
        <div className={css.countryInfo}>
          <span className={css.currency}>{data.currency}</span>
          <span>{data.countryName}</span>
        </div>
      </div>

      <div>
        <span className={css.rateNumber}>{data.exchangeRate?.middle.toFixed(data.precision)}</span>{' '}
        <span>{baseCurrency}</span>
      </div>
    </div>
  );
};
