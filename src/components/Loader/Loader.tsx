import { FC } from 'react';
import css from './Loader.module.css';

export const Loader: FC = () => {
  return (
    <div role="progressbar" className={css.container}>
      <div className={css.loader} />
    </div>
  );
};
