import { FC } from 'react';
import { RatesItemEnhanced } from '../../types/common';
import { CurrencyItem } from '../CurrencyItem/CurrencyItem';
import css from './CurrencyList.module.css';

interface Props {
  data: RatesItemEnhanced[];
  baseCurrency: string;
}

export const CurrencyList: FC<Props> = ({ data, baseCurrency }) => {
  return (
    <ul className={css.wrapper}>
      {data.map((item) => (
        <CurrencyItem key={item.currency} data={item} baseCurrency={baseCurrency} />
      ))}
    </ul>
  );
};
