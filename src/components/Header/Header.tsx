import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import css from './Header.module.css';

export const Header: FC = () => {
  const { t } = useTranslation();
  return (
    <header className={css.wrapper}>
      <h1>{t('header')}</h1>
    </header>
  );
};
