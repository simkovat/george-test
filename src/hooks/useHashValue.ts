import { useEffect, useState } from 'react';

export const useHashValue = () => {
  const urlHash = window.location.hash.replace('#', '');

  const [value, setValue] = useState(urlHash);
  useEffect(() => {
    window.location.hash = value;
  }, [value]);

  return { value, setValue };
};
