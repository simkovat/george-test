import { Flag, RatesData, RatesItem } from '../types/api';
import { RatesItemEnhanced } from '../types/common';

export const rateItemsApiMock: RatesItem[] = [
  {
    currency: 'CZK',
    precision: 2,
    nameI18N: 'Czech Koruna',
    denominations: [5000, 2000, 1000, 500, 200, 100],
    exchangeRate: {
      buy: 25.575,
      middle: 25.925,
      sell: 26.275,
      indicator: 0,
      lastModified: '2018-11-08T23:00:00Z'
    },
    banknoteRate: {
      buy: 24.55,
      middle: 25.9,
      sell: 27.25,
      indicator: 0,
      lastModified: '2018-11-06T23:00:00Z'
    },
    flags: [Flag.Provided]
  },
  {
    currency: 'MXN',
    precision: 2,
    nameI18N: 'Mexican Peso',
    exchangeRate: {
      buy: 22.38,
      middle: 22.98,
      sell: 23.58,
      indicator: 0,
      lastModified: '2018-11-08T23:00:00Z'
    },
    banknoteRate: {
      buy: 21.1,
      middle: 22.6,
      sell: 24.1,
      indicator: 0,
      lastModified: '2018-11-06T23:00:00Z'
    },
    flags: [Flag.Provided]
  },
  {
    currency: 'CUP',
    precision: 0,
    nameI18N: 'Cuban Peso'
  },
  { currency: 'NIO', precision: 2 }
];

export const ratesResponseApiMock: RatesData = {
  institute: 198,
  lastUpdated: '2018-11-09T15:07:00Z',
  comparisonDate: '2018-11-09T12:45:00Z',
  baseCurrency: 'EUR',
  fx: rateItemsApiMock
};

export const transformedRatesMock: RatesItemEnhanced[] = [
  {
    currency: 'CZK',
    precision: 2,
    nameI18N: 'Czech Koruna',
    denominations: [5000, 2000, 1000, 500, 200, 100],
    exchangeRate: {
      buy: 25.575,
      middle: 25.925,
      sell: 26.275,
      indicator: 0,
      lastModified: '2018-11-08T23:00:00Z'
    },
    banknoteRate: {
      buy: 24.55,
      middle: 25.9,
      sell: 27.25,
      indicator: 0,
      lastModified: '2018-11-06T23:00:00Z'
    },
    flags: [Flag.Provided],
    countryIso: 'CZ',
    countryName: 'Czech Republic'
  },
  {
    currency: 'MXN',
    precision: 2,
    nameI18N: 'Mexican Peso',
    exchangeRate: {
      buy: 22.38,
      middle: 22.98,
      sell: 23.58,
      indicator: 0,
      lastModified: '2018-11-08T23:00:00Z'
    },
    banknoteRate: {
      buy: 21.1,
      middle: 22.6,
      sell: 24.1,
      indicator: 0,
      lastModified: '2018-11-06T23:00:00Z'
    },
    flags: [Flag.Provided],
    countryIso: 'MX',
    countryName: 'Mexico'
  }
];

export const searchedRatesMock: RatesItemEnhanced[] = [
  {
    currency: 'CZK',
    precision: 2,
    nameI18N: 'Czech Koruna',
    denominations: [5000, 2000, 1000, 500, 200, 100],
    exchangeRate: {
      buy: 25.575,
      middle: 25.925,
      sell: 26.275,
      indicator: 0,
      lastModified: '2018-11-08T23:00:00Z'
    },
    banknoteRate: {
      buy: 24.55,
      middle: 25.9,
      sell: 27.25,
      indicator: 0,
      lastModified: '2018-11-06T23:00:00Z'
    },
    flags: [Flag.Provided],
    countryIso: 'CZ',
    countryName: 'Czech Republic'
  }
];
