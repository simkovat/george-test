import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { fireEvent, render, screen } from '@testing-library/react';
import { ExchangeRates } from './ExchangeRates';
const queryClient = new QueryClient();

const MockExchangeRates = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <ExchangeRates />
    </QueryClientProvider>
  );
};

describe('Exchange rates search', () => {
  it('urls hash should reflect value', async () => {
    render(<MockExchangeRates />);
    const inputElement = (await screen.findByTestId('search-input')) as HTMLInputElement;
    fireEvent.change(inputElement, { target: { value: 'mexican' } });
    const urlHash = window.location.hash.replace('#', '');
    expect(urlHash).toEqual('mexican');
  });
});
