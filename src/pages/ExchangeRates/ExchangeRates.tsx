import { FC } from 'react';
import { CurrencyList } from '../../components/CurrencyList/CurrencyList';
import { Search } from '../../components/Search/Search';
import { useQuery } from '@tanstack/react-query';
import { getExchangeRates } from '../../api/getExchangeRates';
import { useTranslation } from 'react-i18next';
import { searchRatesData, transformRatesData } from '../../utils/rates';
import { Loader } from '../../components/Loader/Loader';
import { Error } from '../../components/Error/Error';
import { useHashValue } from '../../hooks/useHashValue';
import css from './ExchangeRates.module.css';

export const ExchangeRates: FC = () => {
  const { t } = useTranslation();

  const { data, isLoading, error } = useQuery({
    queryKey: ['exchage-rates'],
    queryFn: getExchangeRates
  });

  const { value, setValue } = useHashValue();

  if (isLoading) return <Loader />;
  if (error) return <Error text={t('general-error')} />;

  if (data) {
    const transformedRates = transformRatesData(data.fx);
    const filteredRates = searchRatesData(transformedRates, value);

    return (
      <div className={css.wrapper}>
        <Search value={value} setValue={setValue} />
        {filteredRates.length > 0 ? (
          <CurrencyList data={filteredRates} baseCurrency={data.baseCurrency} />
        ) : (
          <div>{t('no-results')}</div>
        )}
      </div>
    );
  }
  return null;
};
