import { getISOByParam, getParamByISO } from 'iso-country-currency';

export const getCountryISOFromCurrency = (currency: string) => {
  try {
    const iso = getISOByParam('currency', currency);
    return iso;
  } catch (err) {
    return undefined;
  }
};

export const getCountryNameFromCurrency = (currency: string) => {
  const iso = getCountryISOFromCurrency(currency);
  const countryName = iso && getParamByISO(iso, 'countryName');
  return countryName;
};
