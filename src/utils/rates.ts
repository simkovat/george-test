import { RatesItem } from '../types/api';
import { RatesItemEnhanced } from '../types/common';
import { getCountryISOFromCurrency, getCountryNameFromCurrency } from './countries';

export const transformRatesData = (ratesData: RatesItem[]) =>
  ratesData.reduce<RatesItemEnhanced[]>((result, rate) => {
    const countryIso = getCountryISOFromCurrency(rate.currency);
    const countryName = getCountryNameFromCurrency(rate.currency);

    if (rate.exchangeRate && countryIso && countryName) {
      result.push({ ...rate, countryIso, countryName });
    }

    return result;
  }, []);

export const searchRatesData = (data: RatesItemEnhanced[], searchValue: string) =>
  data.filter(
    (item) =>
      item.countryName.toLowerCase().includes(searchValue.toLowerCase()) ||
      item.currency.toLowerCase().includes(searchValue.toLowerCase()) ||
      item.countryIso.toLowerCase().includes(searchValue.toLowerCase())
  );
