import { searchRatesData, transformRatesData } from '../rates';
import { rateItemsApiMock, searchedRatesMock, transformedRatesMock } from '../../__mocks__/rates';

describe('#transformRatesData ', () => {
  it('filters and enhances exchange rates data', () => {
    expect(transformRatesData(rateItemsApiMock)).toEqual(transformedRatesMock);
  });
});

describe('#searchRatesData', () => {
  it('returns correct search results', () => {
    expect(searchRatesData(transformedRatesMock, 'cz')).toEqual(searchedRatesMock);
  });

  it('returns nothing when search does not exist', () => {
    expect(searchRatesData(transformedRatesMock, 'test')).toEqual([]);
  });
});
