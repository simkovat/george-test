import { getCountryISOFromCurrency, getCountryNameFromCurrency } from '../countries';

describe('#getCountryISOFromCurrency', () => {
  it('returns the correct code for the appropriate input', () => {
    expect(getCountryISOFromCurrency('CZK')).toBe('CZ');
  });

  it('returns the correct code for the appropriate input', () => {
    expect(getCountryISOFromCurrency('MXN')).toBe('MX');
  });

  it('returns undefined when input inapropriate', () => {
    expect(getCountryISOFromCurrency('XXX')).toBeUndefined();
  });
});

describe('#getCountryNameFromCurrency', () => {
  it('returns correct country for the appropriate input', () => {
    expect(getCountryNameFromCurrency('CZK')).toBe('Czech Republic');
  });

  it('returns correct country for the appropriate input', () => {
    expect(getCountryNameFromCurrency('MXN')).toBe('Mexico');
  });
  it('returns correct country for the appropriate input', () => {
    expect(getCountryNameFromCurrency('XXX')).toBeUndefined();
  });
});
