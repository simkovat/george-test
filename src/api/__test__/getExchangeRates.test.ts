import { getExchangeRates } from '../getExchangeRates';
import { ratesResponseApiMock } from '../../__mocks__/rates';

describe('#getExchangeRates', () => {
  it('returns data on sucess', async () => {
    jest.spyOn(global, 'fetch').mockResolvedValue({
      ok: true,
      json: async () => ratesResponseApiMock
    } as Response) as jest.Mock;

    const json = await getExchangeRates();

    expect(json).toEqual(ratesResponseApiMock);
  });

  it('handles exception with null', async () => {
    jest.spyOn(global, 'fetch').mockRejectedValue('API failure') as jest.Mock;

    const response = await getExchangeRates();
    expect(response).toBeNull;
  });
});
