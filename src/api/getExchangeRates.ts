import i18n from '../i18n';
import { RatesData } from '../types/api';

export const getExchangeRates = async (): Promise<RatesData | null> => {
  try {
    const response = await fetch('https://run.mocky.io/v3/c88db14a-3128-4fbd-af74-1371c5bb0343');
    const responseJson = await response.json();

    if (!response.ok) {
      const errorText = i18n.t('general-error');
      throw new Error(errorText);
    }

    return responseJson;
  } catch (err) {
    console.error(err);
    return null;
  }
};
